#include <cstdio>
#include <algorithm>
#include <climits>
#include <forward_list>
#include <list>

using namespace std;

class Vertex{
	private:
		forward_list<unsigned long> adjacent;
	public:
		void addAdjacent(unsigned long vertex);
		void changeChild(unsigned long n);
		unsigned long getChild();
		forward_list<unsigned long>* getAdjacentVertices();
};

/* Add vertex to adjacency list */
void Vertex::addAdjacent(unsigned long vertex){
	this->adjacent.push_front(vertex);
}

/* Get adjacent vertex list */
forward_list<unsigned long>* Vertex::getAdjacentVertices(){
	return &(this->adjacent);
}

class TarjanArticulation{
	private:
		Vertex *graph;
		unsigned long vertices;
		unsigned long *depth;
		unsigned long *parent;
		unsigned long *low;
		unsigned long subGraphs;
		bool *visited;
		bool *cut;
		unsigned long cutVertices;
		unsigned long curVertSubgraph;
		unsigned long maxVertSubgraph;
		forward_list<unsigned long> biggest;
		void visit(unsigned long u, unsigned long time);
		void DFSvisit(unsigned long u);
	public:
		TarjanArticulation(Vertex *graph, unsigned long v);
		~TarjanArticulation();
		void execute();
		unsigned long getCutVertices();
		unsigned long getMaxVertices();
		unsigned long getSubGraphs();
		forward_list<unsigned long>* getBigVertices();
};

// Constructor for Tarjan Articulation
TarjanArticulation::TarjanArticulation(Vertex *graph, unsigned long v){
	this->graph = graph;
	this->vertices = v;
	this->subGraphs = 0;
	this->cutVertices = 0;
	this->curVertSubgraph = 0;
	this->maxVertSubgraph = 0;
	this->depth = new unsigned long[v];
	this->parent = new unsigned long[v];
	this->low = new unsigned long[v];
	this->visited = new bool[v]();
	this->cut = new bool[v]();
}

// Destructor for Tarjan Articulation
TarjanArticulation::~TarjanArticulation(){
	delete [] this->depth;
	delete [] this->parent;
	delete [] this->low;
	delete [] this->visited;
	delete [] this->cut;
}

void TarjanArticulation::execute(){
	unsigned long i;

	for(i = 0; i < this->vertices; i++){
		this->depth[i] = ULONG_MAX;
		this->parent[i] = ULONG_MAX;
	}

	for(i = this->vertices - 1; i != ULONG_MAX; i--){
		if(this->depth[i] == ULONG_MAX){
			this->biggest.push_front(i);
			this->subGraphs++;
			this->visit(i, 0);
		}
	}

	for(i = 0; i < this->vertices; i++){
		if(this->visited[i] == false && this->cut[i] == false){
			this->DFSvisit(i);

			if(this->curVertSubgraph > this->maxVertSubgraph){
				this->maxVertSubgraph = this->curVertSubgraph;
			}

			this->curVertSubgraph = 0;
		}
	}
}

void TarjanArticulation::visit(unsigned long u, unsigned long time){
	this->depth[u] = this->low[u] = time;

	unsigned long child = 0;

	bool isCutVertex = false;
	forward_list<unsigned long> *vertices = this->graph[u].getAdjacentVertices();
	forward_list<unsigned long>::iterator it;

	for(it = vertices->begin(); it != vertices->end(); it++){
		if(this->depth[*it] == ULONG_MAX){
			this->parent[*it] = u;
			this->visit(*it, time + 1);
			child++;

			if(this->low[*it] >= this->depth[u]){
				isCutVertex = true;
			}

			this->low[u] = min(this->low[u], this->low[*it]);
		}else if(*it != this->parent[u]){
			this->low[u] = min(this->low[u], this->depth[*it]);
		}
	}

	if((this->parent[u] == ULONG_MAX && child > 1) || (this->parent[u] != ULONG_MAX && isCutVertex)){
		this->cutVertices++;
		this->cut[u] = true;
	}
}

void TarjanArticulation::DFSvisit(unsigned long u){
	this->curVertSubgraph++;
	this->visited[u] = true;
	forward_list<unsigned long> *vertices = this->graph[u].getAdjacentVertices();
	forward_list<unsigned long>::iterator it;

	for(it = vertices->begin(); it != vertices->end(); it++){
		if(this->visited[*it] == false && this->cut[*it] == false){
			this->DFSvisit(*it);
		}
	}
}

unsigned long TarjanArticulation::getCutVertices(){
	return this->cutVertices;
}

unsigned long TarjanArticulation::getMaxVertices(){
	return this->maxVertSubgraph;
}

unsigned long TarjanArticulation::getSubGraphs(){
	return this->subGraphs;
}

forward_list<unsigned long>* TarjanArticulation::getBigVertices(){
	return &(this->biggest);
}

int main(){
	unsigned long routers;
	unsigned long connections;
	unsigned long i;
	unsigned long orig, dest;

	// Get number of routers and connections
	scanf(" %lu %lu", &routers, &connections);

	Vertex *graph = new Vertex[routers];

	// Get the edges of the network graph
	for(i = 0; i < connections; i++){
		scanf(" %lu %lu", &orig, &dest);

		dest--;
		orig--;

		graph[orig].addAdjacent(dest);
		graph[dest].addAdjacent(orig);
	}

	TarjanArticulation tarjan(graph, routers);
	tarjan.execute();

	forward_list<unsigned long>::iterator it;
	forward_list<unsigned long> *biggest = tarjan.getBigVertices();

	printf("%lu\n", tarjan.getSubGraphs());

	for(it = biggest->begin(); it != biggest->end(); ){
		printf("%lu", *it+1);

		it++;
		if(it != biggest->end()){
			putchar(' ');
		}
	}

	putchar('\n');

	printf("%lu\n", tarjan.getCutVertices());
	printf("%lu\n", tarjan.getMaxVertices());

	delete [] graph;

	return 0;
}

